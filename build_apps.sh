#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJ_DIR="$(realpath "$SCRIPTPATH")"
ELF2HEX_DIR="$(realpath "$SCRIPTPATH/../hsc_evaluator/tools/elf2hex")"
HEX_DIR="$(realpath "$SCRIPTPATH/../../verilator/embench/hex")"

TESTS=" aha-mont64 \
			  crc32 \
			  cubic \
			  edn \
			  huffbench \
			 	matmult-int \
			  minver \
			  nbody \
			  nettle-aes \
			  nettle-sha256 \
			  nsichneu \
			  picojpeg \
        qrduino \
        sglib-combined \
        slre \
        st \
        statemate \
        ud \
        wikisort"

cd $SCRIPTPATH

rm -rf list bd
mkdir -p $HEX_DIR
mkdir -p list

./build_all.py --arch riscv32 --chip generic --board aubrac --cc riscv32-inria-elf-gcc --cflags="-c -march=rv32im -mabi=ilp32 -O2 -ffunction-sections -fdata-sections" --ld riscv32-inria-elf-gcc --ldflags="-Wl,-gc-sections,-T,${PROJ_DIR}/config/riscv32/boards/aubrac/script-riscv32.ld" --user-libs="-lm" --clean

for test in $TESTS; do
	rm $HEX_DIR/${test}.hex 2> /dev/null
	python3 ${ELF2HEX_DIR}/elf2hex.py --input bd/src/$test/$test --output $HEX_DIR/$test.hex --wide 16
	riscv32-inria-elf-objdump -D bd/src/$test/$test > list/$test.list
done
